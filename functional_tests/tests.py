from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_value_resistor_table(self, row_text):
        table = self.browser.find_element_by_id('id_value_resistor_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_row_in_list_color_resistor_table(self, row_text):
        table = self.browser.find_element_by_id('id_color_resistor_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):

        # green เข้ามาใช้งาน
        self.browser.get(self.live_server_url)

        # ตรวจสอบ title ของ webbrowser
        self.assertIn('Convert-Resistor', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text

        # ตรวจสอบหัวข้อหน้าเว็บ
        self.assertIn('Convert-Resistor', header_text)

        # เช็คช่องใส่ข้อมูลสีตัวต้านทาน
        inputboxcolor = self.browser.find_element_by_id('id_new_color')
        self.assertEqual(
                inputboxcolor.get_attribute('placeholder'),
                'Enter a color'
        )

        # เช็คช่องใส่ข้อมูลค่าตัวต้านทาน
        inputboxvalue = self.browser.find_element_by_id('id_new_value')
        self.assertEqual(
                inputboxvalue.get_attribute('placeholder'),
                'Enter a value'
        )

        # พิมค่า 2200 โอม
        inputboxvalue.send_keys('2200')

        # ให้ส่งค่าโดย Enter
        inputboxvalue.send_keys(Keys.ENTER)
        # เช็คดูว่าค่าตรงตาม app ที่เราสร้างมาหรือไม่
        self.check_for_row_in_list_value_resistor_table('2200 ---> red red red')

        # กำหนดให้ inputboxvalue ชี้ที่ id_new_valueและส่งค่า
        inputboxvalue = self.browser.find_element_by_id('id_new_value')
        inputboxvalue.send_keys('1000')
        inputboxvalue.send_keys(Keys.ENTER)

        self.check_for_row_in_list_value_resistor_table('1000 ---> brown black red')
        self.check_for_row_in_list_value_resistor_table('2200 ---> red red red')

        # กำหนดให้ inputboxcolor ชี้ที่ id_new_colorและส่งค่า
        inputboxcolor = self.browser.find_element_by_id('id_new_color')
        inputboxcolor.send_keys('red red red')
        inputboxcolor.send_keys(Keys.ENTER)
        self.check_for_row_in_list_color_resistor_table('red red red ---> 2200')

        green_list_url = self.browser.current_url
        self.assertRegex(green_list_url, '/lists/.+')

        self.browser.quit()
        self.browser = webdriver.Firefox()

        # mean ผู้เข้ามาใช้งานคนใหม่
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('1000 ---> brown black red', page_text)
        self.assertNotIn('2200 ---> red red red', page_text)
        self.assertNotIn('red red red ---> 2200', page_text)


        inputboxvalue = self.browser.find_element_by_id('id_new_value')
        inputboxvalue.send_keys('2000')
        inputboxvalue.send_keys(Keys.ENTER)

        mean_list_url = self.browser.current_url
        self.assertRegex(mean_list_url, '/lists/.+')
        self.assertNotEqual(mean_list_url, green_list_url)

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('1000', page_text)
        self.assertIn('2000', page_text)

        self.fail('Finish the test!')



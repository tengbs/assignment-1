from django.shortcuts import redirect, render
from lists.models import Item

def home_page(request):

    if request.method == 'POST':
        resistor_value = int(request.POST.get('resistor_v',"0"))
        resistor_color = request.POST.get('resistor_c',"")
        if resistor_color == "":
            values = value_to_color(resistor_value)
        elif resistor_value == 0:
            colors = color_to_value(resistor_color)
        return redirect('/')

    items = Item.objects.all()

    return render(request, 'home.html', {'resistorlists': items,})

def value_to_color(resistor_value):
    ten=10
    check=0
    while ten < resistor_value:
        if (resistor_value % ten !=0):
            check=check+1
        ten = ten*10
    if check > 1:
        text_color = "incorrect"
        Item.objects.create(value = resistor_value,color = text_color)
    else:
        value_ = [0,0,0]
        color_ = ["","",""]
        while resistor_value > 99:
            resistor_value = resistor_value/10
            value_[2] = value_[2]+1
        if resistor_value < 100:
            value_[1] = resistor_value%10
            value_[0] = int(resistor_value/10)

        i = 0
        while i<3 :
            if value_[i] == 0:
                color_[i] = 'black'
            elif value_[i] == 1:
                color_[i] = 'brown'
            elif value_[i] == 2:
                color_[i] = 'red'
            elif value_[i] == 3:
                color_[i] = 'orange'
            elif value_[i] == 4:
                color_[i] = 'yellow'
            elif value_[i] == 5:
                color_[i] = 'green'
            elif value_[i] == 6:
                color_[i] = 'blue'
            elif value_[i] == 7:
                color_[i] = 'purple'
            elif value_[i] == 8:
                color_[i] = 'gray'
            elif value_[i] == 9:
                color_[i] = 'white'
            i = i+1
        while value_[2]>0:
            resistor_value = resistor_value*10
            value_[2] = value_[2]-1
        text_color = color_[0]+" "+color_[1]+" "+color_[2]
        Item.objects.create(value = resistor_value,color = text_color)
    return text_color

def color_to_value(resistor_color):
    value = [0,0,0]
    j = 0
    while j<3:
        if resistor_color.split(" ")[j] == 'black':
            value[j] = 0
        elif resistor_color.split(" ")[j] == 'brown':
            value[j] = 1
        elif resistor_color.split(" ")[j] == 'red':
            value[j] = 2
        elif resistor_color.split(" ")[j] == 'orange':
            value[j] = 3
        elif resistor_color.split(" ")[j] == 'yellow':
            value[j] = 4
        elif resistor_color.split(" ")[j] == 'green':
            value[j] = 5
        elif resistor_color.split(" ")[j] == 'blue':
            value[j] = 6
        elif resistor_color.split(" ")[j] == 'purple':
            value[j] = 7
        elif resistor_color.split(" ")[j] == 'gray':
            value[j] = 8
        elif resistor_color.split(" ")[j] == 'white':
            value[j] = 9
        j = j+1
    text_value = (value[0]*10)
    text_value = text_value+value[1]
    text_value = text_value*(10**value[2])
    Item.objects.create(color2 = resistor_color,value2 = text_value)
    return text_value

